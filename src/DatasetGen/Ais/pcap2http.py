#!/usr/bin/env python

import sys
from scapy.all import PcapReader, re, Raw, TCP


VALID_METHODS = [
    "GET",
    "HEAD",
    "POST",
    "PUT",
    "DELETE",
    "CONNECT",
    "OPTIONS",
    "TRACE",
    "PATCH"
]  # see https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods


def payload2http(p):
    lines = re.compile("[\n\r]+").split(p.decode())
    start_line = re.search("^([A-Z]+) ([^ ]+) (HTTP\/[0-9\/]+)", lines[0])
    method = start_line.group(1)
    url = start_line.group(2)
    version = start_line.group(3)  # Never used

    if method not in VALID_METHODS:
        return

    del lines[0]
    headers = []
    body = ""
    for line in lines:
        if "{" in line and "}" in line:
            body = line
            continue
        if ":" in line:
            headers.append(line)
        if "Host:" in line:
            host_header = re.search("^Host: (.*)", line)
            host_name = host_header.group(1)

    request_parameters = [host_name, url, method, '"'+"|".join(headers)+'"']
    return request_parameters, body

def write_to_file(content, payload_name):
    with open("./payloads/"+payload_name, "w") as f:
        f.write(content)


def main():
    if len(sys.argv) != 2:
        print ("I need an pcap input file. Usage ./pcap2http.py pcap-filename")
        return

    infile = sys.argv[1]
    
    payload_counter = 0   

    with PcapReader(infile) as packets:
        for p in packets:
            if p.haslayer(TCP) and p.haslayer(Raw) and p[TCP].dport == 8080:
                payload = p[Raw].load
                req, body = payload2http(payload)
                if req:
                    payload_name = "payload-"+str(payload_counter)
                    print(",".join(req)+","+payload_name)
                    write_to_file(body, payload_name)
                    payload_counter = payload_counter + 1

if __name__ == "__main__":
    main()
