import pika

'''
Get AMQP connection
'''
def get_amqp_connection(sitestore_host, user, password):
    credentials = pika.PlainCredentials(user, password)
    connection = pika.BlockingConnection(pika.ConnectionParameters(
            host=sitestore_host, port="5672",virtual_host="sensor", credentials=credentials))
    return connection 