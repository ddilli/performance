Setup Python Workspace:
$ virtualenv venv
$ source venv/bin/activate

One time dependecy installation:
$ pip install pika
$ pip install ratelimiter

Setup perf rabbitmq user on your sitestore:
$ kubectl -n dragos-sitestore exec -it rabbitmq-external-server-0 -- /bin/sh
$ rabbitmqctl add_user perf
-> use "perf" as password for this user
$ rabbitmqctl set_user_tags perf administrator
$ rabbitmqctl set_permissions -p sensor perf  ".*" ".*" ".*"
$ rabbitmqctl set_permissions -p sensor_pairing perf ".*" ".*" ".*"
$  kubectl -n dragos-sitestore port-forward rabbitmq-external-server-0 5672:5672 --address='0.0.0.0'
