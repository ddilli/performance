import argparse
import functools
import gzip
import io
import json
import os
import pika
import rabbitmq

'''
Global counter that will be used in the amqp callback
'''
payload_counter = 0

'''
Write AMQP Body data to file
'''
def write_to_file(content, filename):
    with open(filename, "w") as f:
        decoded_json = content.decode('utf8').replace("'", '"')
        data = json.loads(decoded_json)
        f.write(json.dumps(data))

'''
Message body consumed from RabbitMq is in gzip format. 
Decompress the gzip data to json
'''
def convert_gzip_to_json(body):
    buf = io.BytesIO(body)
    f = gzip.GzipFile(fileobj=buf)
    decompressed_body = f.read()
    return decompressed_body

def create_payload_dump_directory(path):
    if not os.path.exists(path):
        os.makedirs(path)

'''
Callback method to consume the AMQP messages
'''
def consume_message(channel, method, properties, body, args):
    global payload_counter
    (dump_path) = args
    try:
        '''
        Collect only the deliver.dots.sensorData.ingest - That's the only Exchange we are interestd in.
        '''
        if str(method.routing_key) == "deliver.dots.sensorData.ingest":
            body_string = convert_gzip_to_json(body)
            print(str(method.routing_key) + "|" + str(payload_counter) + "|" +
                str(properties))
            payload_file = "payload-"+str(payload_counter)
            write_to_file(body_string, dump_path+"/payloads/"+payload_file)
            payload_counter = payload_counter + 1
    except Exception as e:
        print(str(e))

def parse_args():
    parser = argparse.ArgumentParser(description="Consume dots messages to dots.sensorData.ingest queue and dump them on disk in a folder called 'payloads'.",
                formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-u', '--user', help='Rabbitmq username', type=str, action='store', required=True)
    parser.add_argument('-p', '--password', help='Rabbitmq password', type=str, action='store', required=True)
    parser.add_argument('-s', '--sitestore', help='Sitestore IP', type=str, action='store', required=True)
    parser.add_argument('-d', '--path', help='Dumb path of the payloads', type=str, action='store', required=True)
    args = vars(parser.parse_args())
    return args

'''
Main will do a blocking connection with the 'dots.sensorData.ingest' 
exchange, consumes a message, writes it to a file until stopped with ctrl+c
'''
def main(args):

    #firehose is the name of the queue where rabbit CCs the traced messages
    queue_name="firehose"
    dump_path = args['path']
    create_payload_dump_directory(dump_path+"/payloads/")

    channel = rabbitmq.get_amqp_connection(args['sitestore'], args['user'], args['password']).channel()
    channel.queue_declare(queue=queue_name, exclusive=False)
    channel.queue_bind(exchange='amq.rabbitmq.trace',
                       queue=queue_name,
                       routing_key="#")
    amqp_consumer_callback = functools.partial(consume_message, args=(dump_path))
    channel.basic_consume("firehose", amqp_consumer_callback)
    
    print("Started consuming messages, press ctrl+c to end")
    channel.start_consuming()

if __name__ == "__main__":
    args = parse_args()
    main(args)
