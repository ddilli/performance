import argparse
import datetime
import gzip
import io
import json
import os
import pika
import rabbitmq
import re
import time
from itertools import cycle
from ratelimiter import RateLimiter

'''
Write AMQP Body data to file
'''
def read_from_file(filename):
    with open(filename, "r") as f:
        data = f.read()
        return data

'''
Message body consumed from RabbitMq is in gzip format. 
Decompress the gzip data to json
'''
def convert_gzip_to_json(body):
    buf = io.BytesIO(body)
    f = gzip.GzipFile(fileobj=buf)
    decompressed_body = f.read()
    return decompressed_body

'''
Update all known fields containing time
TODO: This needs some vetting and proper understanding of the dots schema
'''
def update_time_stamps(payload):
    current_time = datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f000Z")
    payload = re.sub(r'"@timestamp": "\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d+Z"', "\"@timestamp\": \""+current_time+"\"", payload)
    payload = re.sub(r'"start": "\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d+Z"', "\"start\": \""+current_time+"\"", payload)
    payload = re.sub(r'"end": "\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d+Z"', "\"end\": \""+current_time+"\"", payload)
    return payload

'''
Reads dots message payload from a file,
Update all known timestamps to current time

Return a string of the dots json payload
'''
def setup_payload(file, pairing_id):
    content = read_from_file(file)
    content = update_time_stamps(content)
    payload_json = json.loads(content)
    payload_json["observer"]["pairing_id"] = pairing_id
    return json.dumps(payload_json)

def main(args):

    dataset_directory = args['messages'] + "/payloads/"
   
    connection = rabbitmq.get_amqp_connection(args['sitestore'], args['user'], args['password'])
    channel = connection.channel()
    properties=pika.BasicProperties(content_type="application/json", content_encoding="gzip")

    rate_limiter = RateLimiter(max_calls=args['rate'], period=1)

    try: 
        sensor_ids = cycle(args['sensors'])      
        counter = 0
        start_time = time.time()

        while(True):
            for file in os.listdir(dataset_directory):

                payload = setup_payload(dataset_directory+file, next(sensor_ids))
                
                '''
                This is a fairly trivial rate limiter to simulate constant throughput
                TODO: Enhance this to introduce periodic spikes or rate variation based on a distribution
                '''
                with rate_limiter:
                    channel.basic_publish(exchange="sensor", routing_key='data', body=gzip.compress(payload.encode('utf-8')), properties=properties)
                
                # Print some basic progress information
                counter = counter + 1
                if time.time() - start_time >= 60:
                    print(f"Sent {counter} messages in the last 60s")
                    counter = 0
                    start_time = time.time()
    
    except Exception as e:
        '''
        print the exeption and the payload that failed. 
        Continue with other messages.
        '''
        print(f"Failed to publish the content in {file}")
        print(str(e))
    finally:
        connection.close()
    
def parse_args():
    parser = argparse.ArgumentParser(description="Publish dots messages to RabbitMq dots.sensorData.ingest queue on the external instance.",
                formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-r', '--rate', help='Rate of ingesting messages to site store per second', type=int, default=100)
    parser.add_argument('-m', '--messages', help='Path to dots messages dataset directory', type=str, action='store', required=True)
    parser.add_argument('-u', '--user', help='Rabbitmq username', type=str, action='store', required=True)
    parser.add_argument('-p', '--password', help='Rabbitmq password', type=str, action='store', required=True)
    parser.add_argument('-s', '--sitestore', help='Sitestore IP', type=str, action='store', required=True)
    parser.add_argument('-i','--sensors', nargs='+', help='List of sensor parising Ids', required=True)
    args = vars(parser.parse_args())
    return args

if __name__ == "__main__":
    args = parse_args()
    main(args)
